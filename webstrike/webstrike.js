function getIEVersion() {
    var match = navigator.userAgent.match(/(?:MSIE |Trident\/.*; rv:)(\d+)/);
    return match ? parseInt(match[1]) : undefined;
}

jQuery(function($) {
  
  var options  = [342, 341, 345, 346],
      category = $("#cat");

  function queryVar( name, url ) {
    if (!url) url = location.href
    name = name.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");
    var regexS  = "[\?&]"+name+"=([^&#]*)";
    var regex   = new RegExp( regexS );
    var results = regex.exec( url );
    return results == null ? null : results[1];
  }

  $('.ws-nav-item').click(function() {
    var index = $(this).index();

    category.val(options[index]);
    category.change();

    $(this).siblings().removeClass('active');
    $(this).addClass('active');
  });

  $("#searchForm select").change(function() {
    $("#searchForm").submit();
  });

  if( getIEVersion() < 9 && document.cookie.indexOf("browser-warning") < 0 ) {
    $("#browser-alert").addClass("show");
  }

  $("#browser-alert .close").click(function() {
    var expires = new Date();
    
    expires.setTime(expires.getTime() + (24*60*60*1000));
    $("#browser-alert").removeClass("show");
    document.cookie = "browser-warning=false;expires=" + expires;
  });

});