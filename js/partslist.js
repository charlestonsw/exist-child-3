jQuery(function($) {
   
    /*==========================================
    =            Safari Opacity Fix            =
    ==========================================*/

    var ua = navigator.userAgent.toLowerCase(); 

    if (ua.indexOf('safari') != -1 && ua.indexOf('chrome') < 0) {
      $('head').append('<style type="text/css">.fade.in { opacity: 0 !important; }</style>');
    }

});