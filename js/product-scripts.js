jQuery(function($) {
	
	var $window 		  = $(window),
		mainContent 	  = $("#wpo-mainbody > .content-product > .container > .row"),
		sidebarColumn 	  = mainContent.find('> div').first(),
		mainContentColumn = mainContent.find('> section').first(),
		BREAK_TO_MOBILE	  = 991;

	var debounce = function(fn, delay) {
		var timer = null;
	
		return function() {
			clearTimeout(timer);
			timer = setTimeout(fn, delay);
		}
	}

	$.fn.isAfter = function(sel){
		return this.prevAll(sel).length !== 0;
	}
	$.fn.isBefore= function(sel){
		return this.nextAll(sel).length !== 0;
	}

	/*==========  Rearrange Columns for mobile  ==========*/
	
	function reorderColumns() {
		var windowWidth = window.innerWidth;

		if( windowWidth <= BREAK_TO_MOBILE && sidebarColumn.isAfter("") ) {
			sidebarColumn.prependTo(mainContent);
		}
		if( windowWidth > BREAK_TO_MOBILE && sidebarColumn.isBefore("") ) {
			mainContentColumn.prependTo(mainContent);
		}
	}

	var debouncedReorder = debounce(reorderColumns, 500);

	$window.resize(debouncedReorder);
	reorderColumns();

});