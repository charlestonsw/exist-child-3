<?php


/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2014 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */


global $footer_builder;


$footer = of_get_option( 'footer-style' , 'default' );


?>















<?php if ( $footer == 'default' ) { ?>


    <footer id="wpo-footer" class="wpo-footer">


        <div class="container">


            <section class="footer-top">


                <div class="row">


                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">


                        <?php if ( is_active_sidebar( 'footer-1' ) ) : ?>


                            <div class="inner wow fadeInUp" data-wow-duration='0.8s' data-wow-delay="200ms">


                                <?php dynamic_sidebar( 'footer-1' ); ?>


                            </div>


                        <?php endif; ?>


                    </div>


                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">


                        <?php if ( is_active_sidebar( 'footer-2' ) ) : ?>


                            <div class="inner wow fadeInUp" data-wow-duration='0.8s' data-wow-delay="400ms">


                                <?php dynamic_sidebar( 'footer-2' ); ?>


                            </div>


                        <?php endif; ?>


                    </div>


                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">


                        <?php if ( is_active_sidebar( 'footer-3' ) ) : ?>


                            <div class="inner wow fadeInUp" data-wow-duration='0.8s' data-wow-delay="600ms">


                                <?php dynamic_sidebar( 'footer-3' ); ?>


                            </div>


                        <?php endif; ?>


                    </div>


                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">


                        <?php if ( is_active_sidebar( 'footer-4' ) ) : ?>


                            <div class="inner wow fadeInUp" data-wow-duration='0.8s' data-wow-delay="800ms">


                                <?php dynamic_sidebar( 'footer-4' ); ?>


                            </div>


                        <?php endif; ?>


                    </div>


                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">


                        <?php if ( is_active_sidebar( 'footer-5' ) ) : ?>


                            <div class="inner wow fadeInUp" data-wow-duration='0.8s' data-wow-delay="1000ms">


                                <?php dynamic_sidebar( 'footer-5' ); ?>


                            </div>


                        <?php endif; ?>


                    </div>


                </div>


            </section>


        </div>


    </footer>


<?php } else {


    echo $footer_builder[ 'footer' ];


    ?>


    <footer id="wpo-footer" class="wpo-footer">


        <?php echo do_shortcode( get_post( $footer )->post_content ); ?>


    </footer>


    <?php


    echo '</div>';


} ?>


<section class="wpo-copyright">


    <div class="container">


        <?php echo $footer_builder[ 'copyright' ]; ?>


        <div class="copyright">


            <address>Copyright 2017, Bennett Marine Inc. All Rights Reserved


                <!-- <?php echo of_get_option( 'copyright' , 'Copyright &copy; 2017 - Exist theme - All Rights Reserved. <br/> Powered by <a href="http://themeforest.net/user/Opal_WP/?ref=dancof">OpalTheme</a>/ Buy it on <a href="http://themeforest.net/user/Opal_WP/portfolio?ref=dancof">ThemeForest</a>' ); ?> -->


            </address>


        </div>


    </div>


</section>


</section>


<!-- END Wrapper -->


<?php
wp_footer();
$plugin_js_url = get_theme_file_uri( 'js/plugins.js' );
?>
<script type='text/javascript' src='<?= $plugin_js_url ?>'></script>
<script type="text/javascript">
  /* The first line waits until the page has finished to load and is ready to manipulate */
  jQuery( document ).ready( function() {
    /* remove the 'title' attribute of all <img /> tags */
    jQuery( 'img' ).removeAttr( 'title' );
  } );
</script>
</body>
</html>