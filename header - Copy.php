<!DOCTYPE html>
<!--Craig's Changelog-->

<!--- Removed theme topbar and header dynamic section and replaced with hand coded divs, pasting in uber menu manual integration code-->

<?php

/**

 * $Desc

 *

 * @version    $Id$

 * @package    wpbase

 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>

 * @copyright  Copyright (C) 2014 wpopal.com. All Rights Reserved.

 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html

 *

 * @website  http://www.wpopal.com

 * @support  http://www.wpopal.com/support/forum.html

 */

?>

<!--[if IE 7]>

<html class="ie ie7" <?php language_attributes(); ?>>

<![endif]-->

<!--[if IE 8]>

<html class="ie ie8" <?php language_attributes(); ?>>

<![endif]-->

<!--[if !(IE 7) | !(IE 8)  ]><!-->

<html <?php language_attributes(); ?>>

<!--<![endif]-->
<head>

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <?php wp_head(); ?>
    
    <!--[if IE 9 ]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <!--<![endif]-->

</head>


<body


 <?php
 $more_classes = preg_match('/(?i)msie 9/',$_SERVER['HTTP_USER_AGENT']) ? "som-ie9" : "";
 body_class($more_classes);
 ?>>
 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52503241-1', 'auto');
  ga('send', 'pageview');

</script>

  <div id="browser-alert">
    You are using an older version of the Internet Explorer web browser. For the best experience, please <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">click here</a> to get the latest version.<span>If you continue to experience any issues, please contact headquarters at 954-427-1400.</span>
    <div class="close">x</div>
  </div>

    <?php

        $meta_template = get_post_meta(get_the_ID(),'wpo_template',true);

    ?>



    <!-- START Wrapper -->

	<section class="wpo-wrapper <?php echo isset($meta_template['el_class']) ? $meta_template['el_class'] : '' ; ?>">



		<!-- Top bar -->
        

        
        <Div id="topBarCD">
           <?php ubermenu( 'main' , array( 'theme_location' => 'topmenu' ) ); ?>
         </Div>



		<!-- // Topbar -->

		
        
        <!-- CD HEADER -->

		<div id="mainNavContianerCD">
        <div id="headerCD">
      		<?php ubermenu( 'main' , array( 'menu' => 303 ) ); ?>
        </div>
        </div>

		<!-- CD //HEADER -->
        
        <!-- OPAL HEADER -->

    <header id="wpo-header" class="wpo-header"><div class="container">

      <div class="container-inner header-wrap clearfix">

            <div class="header-wrapper-inner">

      </div></div></div>

    </header>

<!-- OPAL //HEADER -->