<?php

define('BTT_THEME_URI', get_stylesheet_directory_uri());

define('BTT_THEME_DIRECTORY', get_stylesheet_directory());

/**
 * Placeholders for things that may be missing if plugins/themes are not loaded.
 */
if ( ! function_exists( 'ubermenu' ) ) {
    function ubermenu() {}
}



/*=============================================

=            Add Custom Scripts               =

=============================================*/

function add_btt_scripts() {

	wp_register_script('btt-product-scripts', BTT_THEME_URI . '/js/product-scripts.js', array('jquery'), 1.0, true);

	wp_register_script('btt-partslist', BTT_THEME_URI . '/js/partslist.js', array('jquery'), 1.0, true);

	

	if( is_archive('product') || is_singular('product') ) {

		wp_enqueue_script('btt-product-scripts');

	}

	if( is_page('products-list') ) {

		wp_enqueue_script('btt-partslist');

	}

	if( is_page('find-dealers-distributors-list-view') ) {

		wp_enqueue_script('btt-partslist');

	}



}

add_action('wp_enqueue_scripts', 'add_btt_scripts');



function add_ws_scripts() {

	wp_register_script('ws-scripts', BTT_THEME_URI . '/webstrike/webstrike.js', array('jquery'));



	wp_enqueue_style('ws-style', BTT_THEME_URI . '/webstrike/webstrike.css');

	wp_enqueue_script('ws-scripts');

}

add_action('wp_enqueue_scripts', 'add_ws_scripts');



function add_store_sort() {

	wp_register_script('tinysort', BTT_THEME_URI . '/webstrike/js/tinysort.js', array('jquery'));

	

	if( is_page('find-a-dealer') ) {

		wp_enqueue_script("tinysort");

	}

}

add_action('wp_enqueue_scripts', 'add_store_sort');

/* WP Curve - #94364 */

add_action('wp_footer','wpc_tracking_code');

function wpc_tracking_code(){

?>

<script type="text/javascript">

/* <![CDATA[ */

var google_conversion_id = 950643971;

var google_custom_params = window.google_tag_params;

var google_remarketing_only = true;

/* ]]> */

</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">

</script>

<noscript>

<div style="display:inline;">

<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/950643971/?value=0&guid=ON&script=0"/>

</div>

</noscript>

<?php

}

/* WP Curve - #94364 - end */





/* WP Curve MC 103304 */

add_action( 'woocommerce_after_shop_loop_item_title', 'shop_sku' );

function shop_sku(){

global $product;

echo '<span itemprop="productID" class="sku">' . $product->sku . '</span>';

} 



/* WP Curve MC 103304 */







function translate_woocommerce($translation, $text, $domain) {

if ($domain == 'woocommerce') {

switch ($text) {

case 'SKU':

$translation = 'Mfg. PN#';

break;



case 'SKU:':

$translation = 'Mfg. PN#:';

break;



}



}   



return $translation;



}

add_filter('gettext', 'translate_woocommerce', 10, 3);



/* WP Curve - Tunbosun - #103942 */

function wpc_103942_add_hotjar_tracking(){

?>

    <!-- Hotjar Tracking Code for http://www.bennetttrimtabs.com -->

    <script>

        (function(h,o,t,j,a,r){

            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};

            h._hjSettings={hjid:135174,hjsv:5};

            a=o.getElementsByTagName('head')[0];

            r=o.createElement('script');r.async=1;

            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;

            a.appendChild(r);

        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');

    </script>

<?php

}

add_action( 'wp_head', 'wpc_103942_add_hotjar_tracking' );

/* WP Curve - Tunbosun - #103942 - end */

/* WP Curve AB #179947 Start */

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 60 );

/* WP Curve AB #179947 End */


/* Craig remove related products 3/31/17 Start */

function wc_remove_related_products( $args ) {
	return array();
}
add_filter('woocommerce_related_products_args','wc_remove_related_products', 10); 

/* Craig remove related products 3/31/17 End */



// Change the description tab title to product name
add_filter( 'woocommerce_product_tabs', 'wc_change_product_description_tab_title', 10, 1 );
function wc_change_product_description_tab_title( $tabs ) {
	if ( isset( $tabs['description']['title'] ) )
		$tabs['description']['title'] = 'Description & Features';
	return $tabs;
}
// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array( 'wpo-woocommerce' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

// END ENQUEUE PARENT ACTION
