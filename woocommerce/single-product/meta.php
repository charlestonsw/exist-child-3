<?php
/**
 * Single Product Meta
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<div class="sku_wrapper"><?php _e( 'SKU:', 'woocommerce' ); ?> <span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span>.</div>

	<?php endif; ?>

	<?php echo $product->get_categories( ', ', '<div class="posted_in">' . _n( 'Category:', 'Categories:', $cat_count, 'woocommerce' ) . ' ', '</div>' ); ?>

	<?php echo $product->get_tags( ', ', '<div class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', '.</div>' ); ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
<div class="2buttons">
	<div class="vc_button-2-wrapper vc_button-2-align-inline">
		<a target="" title="" href="/find-a-dealer/" class="vc_btn vc_btn_sky vc_btn-sky vc_btn_md vc_btn-md vc_btn_outlined">Find a Dealer</a>
	</div>
	<div class="vc_button-2-wrapper vc_button-2-align-inline">
		<a target="" title="" href="/contact/" class="vc_btn vc_btn_sky vc_btn-sky vc_btn_md vc_btn-md vc_btn_outlined">Contact Us For More Info</a>
	</div>
</div>
