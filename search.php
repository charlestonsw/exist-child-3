<?php

/**

 * $Desc

 *

 * @version    $Id$

 * @package    wpbase

 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>

 * @copyright  Copyright (C) 2014 wpopal.com. All Rights Reserved.

 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html

 *

 * @website  http://www.wpopal.com

 * @support  http://www.wpopal.com/support/forum.html

 */

$template = new WPO_Template();

$config = $template->configLayout(of_get_option('single-layout','0-1-0'));



?>



<?php get_header( $wpo->getHeaderLayout() ); ?>



<?php wpo_breadcrumb(); ?>

<section id="wpo-mainbody" class="wpo-mainbody clearfix main-page">

    <div class="container">

        <div class="row">

            <!-- MAIN CONTENT -->

            <div class="<?php echo $config['main']['class']; ?>">

                <div id="wpo-content" class="wpo-content">

                    <?php  if ( have_posts() ) : ?>

                        <div class="post-area">

                            <?php while ( have_posts() ) : the_post(); ?>

                                <?php get_template_part( 'templates/blog/blog'); ?>

                            <?php endwhile; ?>

                        </div>

                        <?php global $wp_query; ?>

                    <?php else : ?>

                        <?php get_template_part( 'templates/none' ); ?>

                    <?php endif; ?>

                </div>

            </div>

            <?php /******************************* SIDEBAR RIGHT ************************************/ ?>

            <?php if($config['right-sidebar']['show']){ ?>

                <div class="<?php echo $config['right-sidebar']['class']; ?>">

                    <div class="wpo-sidebar wpo-sidebar-right">

                        <?php if(is_active_sidebar(of_get_option('right-sidebar'))): ?>

                        <div class="sidebar-inner">

                            <?php dynamic_sidebar(of_get_option('right-sidebar')); ?>

                        </div>

                        <?php endif; ?>

                    </div>

                </div>

            <?php } ?>

            <?php /******************************* END SIDEBAR RIGHT *********************************/ ?>


        </div>

    </div>

</section>



<?php get_footer(); ?>